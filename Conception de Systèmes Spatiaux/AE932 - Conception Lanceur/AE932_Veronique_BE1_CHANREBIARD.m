%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                         %
%                          Conception lanceur                             %
%                              BE n�1                                     %
%                             08/10/19                                    %
%                         CHAN/REBIARD--CREPIN                            %
%                                                                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
clear all; close all; clc;
set(0,'defaultfigurecolor',[.83 .93 .96])
%
%
%% Constantes
g=9;
n_max=5;
lambda=5;
ve=2700;
%% Premi�re partie: D�collage
tc=ve*(lambda-1)/(g*(n_max+1));                                         % Temps de combustion [s]
v1=ve*log(lambda)-g*tc;                                                 % Vitesse de la fus�e en fin de combustion [m/s]
%
%% Seconde partie: Phase balistique
z=-g/2*tc^2+ve*tc*(1-log(lambda)/(lambda-1));                           % Altitude � la fin de combustion [m]
H=ve^2/g*(1/(n_max+1)*(lambda*(1-log(lambda))-1)+0.5*log(lambda)^2);    % Altitude maximale [m]

fprintf('\t v1 [m/s]\t|\t z [km] \t|\tH [km]\n')
fprintf('   -------------|---------------|----------\n')
fprintf('\t %6.1f \t|\t   %5.1f \t|\t%5.1f\n',v1,z/1000,H/1000)
%
lambda=1:0.1:10;
Y=(1/(n_max+1)*(lambda.*(1-log(lambda))-1)+0.5*log(lambda).^2);
figure(1)
plot(lambda,Y,'linewidth',2); grid;
title('Fig.1 : Courbe f(\lambda)')
xlabel('\lambda');ylabel('Y')