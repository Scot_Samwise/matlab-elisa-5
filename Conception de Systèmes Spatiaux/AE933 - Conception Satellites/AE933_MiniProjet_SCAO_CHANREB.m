%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                         %
%                          Mini-projet SCAO                               %
%                             08/10/19                                    %
%                         CHAN/REBIARD--CREPIN                            %
%                                                                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
clear all; close all; clc;
set(0,'defaultfigurecolor',[.83 .93 .96])
%
%% I - Mod�lisation boucle ouverte
%
% D�clarations des constantes
Js=13.6;                        % Moment d'inertie du satellite incluant la roue d'inertie [kg.m^2]
Jw=1.36e-4;                     % Moment d'inertie de la roue d'inertie [kg.m^2]
Bw=1.01e-6;                     % Frottements visqueux entre la roue et le satellite [N.m.s/rad]
J_eq=Jw*Js/(Jw+Js);             % Moment d'inertie �quivalent
%
% Matrices A,B,C et D
A=[0 1 0 ; 0 0 Bw/Js ; 0 0 -Bw/J_eq];
B=1/Js* [0 0; -1 1 ; Js/J_eq -1];
C=30/pi*[6 0 0 ; 0 0 1];
D=0;
D1=zeros(1,2);
D2=zeros(2,2);
%
% Q3 - Matrice de transfert (2 entr�es, 2 sorties)
sat_ss=ss(A,B,C,D2,'StateName', {'Theta(t)','w(t)','W(t)'});
set(sat_ss,'InputName',{'Couple moteur(t)','Perturbations'},'OutputName',{'Thetam(t)','W(t)'});
sat_tf=minreal(tf(sat_ss))
%
% Q4 - R�ponses indicielles 
G_ct=sat_tf(1,1);       % Couple moteur theta
G_cw=sat_tf(2,1);       % Couple moteur omega
G_dt=sat_tf(1,2);       % Couple moteur perturbation theta
G_dw=sat_tf(2,2);       % Couple moteur perturbation omega
%
% Extraction des num�rateurs et d�nominateurs
[numG_ct,denG_ct] = tfdata(G_ct,'v');
[numG_cw,denG_cw] = tfdata(G_cw,'v');
[numG_dt,denG_dt] = tfdata(G_dt,'v');
[numG_dw,denG_dw] = tfdata(G_dw,'v');
%
% Sauvegarde
save sat.mat A B C D D1 D2 G_ct G_cw G_dt G_dw numG_ct numG_cw numG_dt numG_dw ...
    denG_ct denG_dt denG_cw denG_dw
%
% Trac� fig 1
t=[0:2:1000];
disp('G_ct(s): Couple moteur --> Angle de pointage')
G_ct = minreal(tf(numG_ct,denG_ct))
taum=1e-4*ones(size(t));
y=lsim(G_ct,taum,t);
figure(1)
plot(t,y,'linewidth',2); grid;
ylabel('\bfDegr�s');xlabel('\bfSecondes');
title('\bfFig.1 : Boucle ouverte : \theta(t) d� � \tau_m(t) = 1.0e-4 Nm',...
    'fontsize',12)
% Trac� fig 2
t=[0:2:1000];
disp('G_cw(s): Couple moteur --> Vitesse de la roue')
G_cw = minreal(tf(numG_cw,denG_cw))
taum=1e-4*ones(size(t));
y=lsim(G_cw,taum,t);
figure(2)
plot(t,y,'linewidth',2); grid;
ylabel('\bfDegr�s');xlabel('\bfSecondes');
title('\bfFig.2 : Boucle ouverte : \Omega d� � \tau_m(t) = 1.0e-4 Nm',...
    'fontsize',12)
% Trac� fig 3
disp('G_dt(s): Couple de perturbation --> Angle de pointage')
G_dt = minreal(tf(numG_dt,denG_dt))
y=1e-4*step(G_dt,t);
figure(3)
plot(t,y,'linewidth',2); grid;
ylabel('\bfDegr�s');xlabel('\bfSecondes');
title('\bfFig.3 : Boucle ouverte : \theta(t) d� � \tau_m(t) = 1.0e-4 Nm',...
    'fontsize',12)
% Trac� fig 4
disp('G_dw(s): Couple de perturbation --> Vitesse de la roue')
G_dw = minreal(tf(numG_dw,denG_dw))
y=1e-4*step(G_dw,t);
figure(4)
plot(t,y,'linewidth',2); grid;
ylabel('\bfDegr�s');xlabel('\bfSecondes');
title('\bfFig.4 : Boucle ouverte : \Omega d� � \tau_m(t) = 1.0e-4 Nm',...
    'fontsize',12)
%
%% Q5 - Z�ros, p�les, facteur de gain
noms{1}='G_ct';
noms{2}='G_cw';
noms{3}='G_dt';
noms{4}='G_dw';

for k=1:4
eval(noms{k})
eval(['[z',num2str(k),',p',num2str(k),',k',num2str(k),']=zpkdata(',noms{k},')'])
eval(['[res',num2str(k),',pp',num2str(k),',other',num2str(k),']=residue(num',noms{k},',den',noms{k},')'])
end
%
% Q6 - Diagramme de Bode
w=logspace(-4,0,1000);
figure(1); bode(-G_ct,w);
title('\bfFig.1 : Diagramme de Bode de -G_ct','fontsize',12); grid
figure(2); bode(-G_cw,w);
title('\bfFig.2 : Diagramme de Bode de -G_cw','fontsize',12); grid
figure(3); bode(-G_dt,w);
title('\bfFig.3 : Diagramme de Bode de -G_dt','fontsize',12); grid
figure(4); bode(-G_dw,w);
title('\bfFig.4 : Diagramme de Bode de -G_dw','fontsize',12); grid
%% II - Correction cascade de type P (proportionnelle)
%
% Q7 - R�glage du correcteur � action proportionnelle
G_ct
zeta=-log(0.15)/sqrt(pi^2+log(0.15)^2)
Kp=-(2*zeta/0.007427)^-2/4.213
%
sisotool(-G_ct,-Kp) % Attention au signe
%
Kp_str = num2str(Kp); numGc=Kp; denGc=1;
Gc=tf(numGc,denGc)
G_OL = Gc*G_ct
G_CL = feedback(G_OL,1)
%
t=[0:2:1500]; y = step(G_CL,t);
figure(5); plot(t,y,'linewidth',2); grid
xlabel('\bfTemps (s)');ylabel('\bf\theta angle de pointage (�)')
title('\bfFig.5 : \theta(t) avec Cor. Prop. pour \theta_r = 1�','fontsize',12)
text(500,0.8,['\bfKp=' Kp_str])
%
figure(6)
w=logspace(-3,-1,1000); bodemag(G_CL,w); grid;
title('\bfFig.6 : Diagramme de Bode de la FTBF','fontsize',12)
%
% Bande passante
disp('Bande passante � -6dB de la FTBF = ')
wc_6dB=bandwidth(G_CL,-6)
%
% Marges
[Gm,Pm,Wcg,Wcp] = margin(G_CL)
%
% Pulsation de r�sonnance et QdB
%
% R�ponse indicielle
figure(7);
step(G_CL,t);grid;
title(['\bfFig.7 : R�ponse indicielle de G_{CL} avec K_p=',num2str(Kp)])